module.exports = {
  setupFiles: [],
  testMatch: ['<rootDir>/**/*.test.js'],
  collectCoverageFrom: ['<rootDir>/src/**/*.js', '!<rootDir>/node_modules/*']
};
