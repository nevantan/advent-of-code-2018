import { test_sorted, test_random, real_data } from './day4.fixture';
import {
  parseLine,
  guardReducer,
  findSleepHigh,
  findHighestMinute,
  highMinuteAllGuards
} from './';

describe('day4', () => {
  it('returns a correct action object', () => {
    expect(parseLine(test_sorted[0])).toEqual({
      timestamp: new Date('1518-11-01 00:00'),
      id: 'set_active',
      guard: 10
    });

    expect(parseLine(test_sorted[1])).toEqual({
      timestamp: new Date('1518-11-01 00:05'),
      id: 'fall_asleep'
    });

    expect(parseLine(test_sorted[2])).toEqual({
      timestamp: new Date('1518-11-01 00:25'),
      id: 'wake_up'
    });
  });

  it('transforms the guard object properly', () => {
    const states = [
      parseLine(test_sorted[0]),
      parseLine(test_sorted[1]),
      parseLine(test_sorted[2])
    ];
  });

  it('returns the guard id with most sleep time', () => {
    const actions = test_sorted.map((line) => parseLine(line));

    const state = actions.reduce((state, action) => {
      return guardReducer(state, action);
    }, guardReducer(undefined, { id: '@INIT' }));

    expect(findSleepHigh(state)).toBe(10);
  });

  it('returns the minute with the highest frequency', () => {
    const actions = test_sorted.map((line) => parseLine(line));

    const state = actions.reduce((state, action) => {
      return guardReducer(state, action);
    }, guardReducer(undefined, { id: '@INIT' }));

    expect(findHighestMinute(state, findSleepHigh(state))).toBe(24);
  });

  it('solves strategy 1', () => {
    const actions = real_data.map((line) => parseLine(line));
    actions.sort((a, b) => {
      return a.timestamp - b.timestamp;
    });

    const state = actions.reduce((state, action) => {
      return guardReducer(state, action);
    }, guardReducer(undefined, { id: '@INIT' }));

    const highSleepGuard = findSleepHigh(state);
    const highMinute = findHighestMinute(state, highSleepGuard);
    //console.log(highSleepGuard * highMinute);
  });

  it('finds the highest minute of all guards', () => {
    const actions = test_sorted.map((line) => parseLine(line));

    const state = actions.reduce((state, action) => {
      return guardReducer(state, action);
    }, guardReducer(undefined, { id: '@INIT' }));

    expect(highMinuteAllGuards(state)).toEqual({
      guard: 99,
      minute: 45
    });
  });

  it('solves strategy 2', () => {
    const actions = real_data.map((line) => parseLine(line));
    actions.sort((a, b) => {
      return a.timestamp - b.timestamp;
    });

    const state = actions.reduce((state, action) => {
      return guardReducer(state, action);
    }, guardReducer(undefined, { id: '@INIT' }));

    const result = highMinuteAllGuards(state);
    console.log(result);
  });
});

describe('guard reducer', () => {
  it('returns the default state', () => {
    expect(guardReducer(undefined, {})).toEqual({
      active: -1,
      guards: {}
    });
  });

  it('sets an active guard', () => {
    let action = parseLine(test_sorted[0]);
    let state = guardReducer(undefined, { id: '@INIT' });
    state = guardReducer(state, action);

    expect(state).toEqual({
      active: 10,
      guards: {}
    });
  });

  it('sets a sleep start time', () => {
    let activate = parseLine(test_sorted[0]);
    let sleep = parseLine(test_sorted[1]);
    let state = guardReducer(undefined, { id: '@INIT' });
    state = guardReducer(state, activate);
    state = guardReducer(state, sleep);

    expect(state).toEqual({
      active: 10,
      guards: {
        10: {
          id: 10,
          sleepStart: sleep.timestamp
        }
      }
    });
  });

  it('calculates sleep time on end', () => {
    let activate = parseLine(test_sorted[0]);
    let sleep = parseLine(test_sorted[1]);
    let wake = parseLine(test_sorted[2]);

    let state = guardReducer(undefined, { id: '@INIT' });
    state = guardReducer(state, activate);
    state = guardReducer(state, sleep);
    state = guardReducer(state, wake);

    expect(state).toEqual({
      active: 10,
      guards: {
        10: {
          id: 10,
          sleepStart: null,
          sleepTime: {
            5: 1,
            6: 1,
            7: 1,
            8: 1,
            9: 1,
            10: 1,
            11: 1,
            12: 1,
            13: 1,
            14: 1,
            15: 1,
            16: 1,
            17: 1,
            18: 1,
            19: 1,
            20: 1,
            21: 1,
            22: 1,
            23: 1,
            24: 1
          }
        }
      }
    });
  });

  it('handles multiple transformations', () => {
    const actions = test_sorted.map((line) => parseLine(line));

    const state = actions.reduce((state, action) => {
      return guardReducer(state, action);
    }, guardReducer(undefined, { id: '@INIT' }));

    expect(state).toEqual({
      active: 99,
      guards: {
        10: {
          id: 10,
          sleepStart: null,
          sleepTime: {
            5: 1,
            6: 1,
            7: 1,
            8: 1,
            9: 1,
            10: 1,
            11: 1,
            12: 1,
            13: 1,
            14: 1,
            15: 1,
            16: 1,
            17: 1,
            18: 1,
            19: 1,
            20: 1,
            21: 1,
            22: 1,
            23: 1,
            24: 2,
            25: 1,
            26: 1,
            27: 1,
            28: 1,
            30: 1,
            31: 1,
            32: 1,
            33: 1,
            34: 1,
            35: 1,
            36: 1,
            37: 1,
            38: 1,
            39: 1,
            40: 1,
            41: 1,
            42: 1,
            43: 1,
            44: 1,
            45: 1,
            46: 1,
            47: 1,
            48: 1,
            49: 1,
            50: 1,
            51: 1,
            52: 1,
            53: 1,
            54: 1
          }
        },
        99: {
          id: 99,
          sleepStart: null,
          sleepTime: {
            36: 1,
            37: 1,
            38: 1,
            39: 1,
            40: 2,
            41: 2,
            42: 2,
            43: 2,
            44: 2,
            45: 3,
            46: 2,
            47: 2,
            48: 2,
            49: 2,
            50: 1,
            51: 1,
            52: 1,
            53: 1,
            54: 1
          }
        }
      }
    });
  });
});
