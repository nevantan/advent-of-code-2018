const parseLine = (str) => {
  const groups = str.match(/^\[(\d{4}-\d{2}-\d{2} \d{2}:\d{2})] (.*)$/);

  let action_line = groups[2].split(' ');
  switch (action_line[0]) {
    case 'Guard':
      return {
        timestamp: new Date(groups[1]),
        guard: parseInt(action_line[1].replace('#', '')),
        id: 'set_active'
      };
    case 'falls':
      return {
        timestamp: new Date(groups[1]),
        id: 'fall_asleep'
      };
    case 'wakes':
      return {
        timestamp: new Date(groups[1]),
        id: 'wake_up'
      };
  }
};

const findSleepHigh = (state) => {
  return Object.values(state.guards).reduce(
    (state, guard) => {
      const time = Object.values(guard.sleepTime).reduce(
        (acc, minutes) => acc + minutes,
        0
      );
      if (time > state.high)
        return {
          id: guard.id,
          high: time
        };

      return state;
    },
    {
      id: -1,
      high: 0
    }
  ).id;
};

const findHighestMinute = (state, id) => {
  return Object.entries(state.guards[id].sleepTime).reduce(
    (acc, minute) => {
      if (minute[1] > acc.val)
        return {
          minute: parseInt(minute[0]),
          val: minute[1]
        };
      return acc;
    },
    {
      val: 0,
      minute: 0
    }
  ).minute;
};

const highMinuteAllGuards = (state) => {
  const result = Object.values(state.guards).reduce(
    (state, guard) => {
      const minutes = Object.entries(guard.sleepTime);
      const minute = minutes.reduce(
        (high, minute) => {
          if (minute[1] > high.val) {
            return {
              minute: parseInt(minute[0]),
              val: minute[1]
            };
          }
          return high;
        },
        {
          minute: 0,
          val: 0
        }
      );

      if (minute.val > state.high) {
        return {
          id: guard.id,
          high: minute.val,
          minute: minute.minute
        };
      }

      return state;
    },
    {
      id: -1,
      high: 0,
      minute: 0
    }
  );

  return {
    guard: result.id,
    minute: result.minute
  };
};

const initialState = {
  active: -1,
  guards: {}
};
const guardReducer = (state = initialState, action) => {
  switch (action.id) {
    case 'set_active':
      return {
        ...state,
        active: action.guard
      };
    case 'fall_asleep':
      return {
        ...state,
        guards: {
          ...state.guards,
          [state.active]: {
            ...state.guards[state.active],
            id: state.active,
            sleepStart: action.timestamp
          }
        }
      };
    case 'wake_up':
      let startTime = state.guards[state.active].sleepStart;
      const sleepTime = { ...state.guards[state.active].sleepTime };
      for (
        let i = startTime.getMinutes();
        i < action.timestamp.getMinutes();
        i++
      ) {
        if (!sleepTime[i]) {
          sleepTime[i] = 1;
        } else {
          sleepTime[i]++;
        }
      }

      return {
        ...state,
        guards: {
          ...state.guards,
          [state.active]: {
            ...state.guards[state.active],
            sleepStart: null,
            sleepTime
          }
        }
      };
    default:
      return state;
  }
};

export {
  parseLine,
  guardReducer,
  findSleepHigh,
  findHighestMinute,
  highMinuteAllGuards
};
