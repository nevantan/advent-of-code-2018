import { points, real } from './day6.fixture';
import {
  Point,
  searchSpace,
  candidates,
  searchWhich,
  ownership,
  safeRegion
} from './';

describe('point class', () => {
  it('stores an x and y point', () => {
    const p = new Point(1, 2);
    expect(p.x).toBe(1);
    expect(p.y).toBe(2);
  });

  it('returns the distance between itself and another point', () => {
    const p1 = new Point(1, 1);
    const p2 = new Point(8, 3);
    expect(p1.dist(p2)).toBe(9);
  });
});

describe('day 6', () => {
  it('finds the search space', () => {
    expect(searchSpace(points)).toEqual({
      x: 1,
      y: 1,
      width: 8,
      height: 9
    });
  });

  it('returns the candidate points', () => {
    expect(candidates(points)).toEqual([points[3], points[4]]);
  });

  it('returns the real candidates', () => {
    //console.log(candidates(real));
  });

  it('determines search points', () => {
    expect(searchWhich([...points], new Point(3, 2))).toEqual([
      points[0],
      points[2],
      points[3]
    ]);
  });

  it('assigns ownership for each point in the search space', () => {
    const counts = ownership(points);
    expect(counts).toEqual([
      new Point(points[0].x, points[0].y, points[0].id, 7, false),
      new Point(points[1].x, points[1].y, points[1].id, 9, false),
      new Point(points[2].x, points[2].y, points[2].id, 12, false),
      new Point(points[3].x, points[3].y, points[3].id, 9, true),
      new Point(points[4].x, points[4].y, points[4].id, 17, true),
      new Point(points[5].x, points[5].y, points[5].id, 10, false)
    ]);
  });

  it.skip('solves the puzzle', () => {
    const points = ownership(real);
    console.log(points.length);
    const possible = candidates(real);
    const max = points
      .filter((point) => point.valid)
      .reduce((max, point) => (point.count > max ? point.count : max), 0);
    console.log(max);
  });

  it('finds the safe region', () => {
    const region = safeRegion(points, 30);
    expect(region).toEqual([
      new Point(3, 3),
      new Point(4, 3),
      new Point(5, 3),
      new Point(2, 4),
      new Point(3, 4),
      new Point(4, 4),
      new Point(5, 4),
      new Point(6, 4),
      new Point(2, 5),
      new Point(3, 5),
      new Point(4, 5),
      new Point(5, 5),
      new Point(6, 5),
      new Point(3, 6),
      new Point(4, 6),
      new Point(5, 6)
    ]);
  });

  it('finds the real safe region', () => {
    const region = safeRegion(real, 10000);
    console.log(region.length);
  });
});
