class Point {
  constructor(x, y, id = '_', count = 0, valid = true) {
    this._x = x;
    this._y = y;
    this._id = id;
    this._count = count;
    this._valid = valid;
  }

  dist(p) {
    return Math.abs(this._x - p.x) + Math.abs(this._y - p.y);
  }

  get x() {
    return this._x;
  }

  get y() {
    return this._y;
  }

  get id() {
    return this._id;
  }

  get count() {
    return this._count;
  }

  set count(val) {
    this._count = val;
  }

  get valid() {
    return this._valid;
  }

  set valid(val) {
    this._valid = val;
  }

  static sortX(a, b) {
    if (a.x < b.x) return -1;
    if (a.x > b.x) return 1;
    return 0;
  }

  static sortY(a, b) {
    if (a.y < b.y) return -1;
    if (a.y > b.y) return 1;
    return 0;
  }
}

const searchSpace = (points) => {
  const ps = [...points];
  ps.sort(Point.sortX);

  const lowX = ps[0];
  const highX = ps[ps.length - 1];

  ps.sort(Point.sortY);
  const lowY = ps[0];
  const highY = ps[ps.length - 1];

  return {
    x: lowX.x,
    y: lowY.y,
    width: highX.x,
    height: highY.y
  };
};

const candidates = (points) => {
  let ps = [...points];

  ps.sort(Point.sortX);
  ps = ps.filter(
    (point) =>
      point.x !== points[0].x && point.x !== points[points.length - 1].x
  );

  ps.sort(Point.sortY);
  ps = ps.filter(
    (point) =>
      point.y !== points[0].y && point.y !== points[points.length - 1].y
  );

  return ps;
};

const searchWhich = (points, point) => {
  const ps = [...points];
  ps.sort((a, b) => Math.abs(point.x - a.x) - Math.abs(point.x - b.x));

  const lowX = ps.filter((p, i, arr) => {
    return (
      Math.min(Math.abs(point.x - p.x), Math.abs(p.x - point.x)) <=
      Math.min(Math.abs(point.x - arr[0].x), Math.abs(arr[0].x - point.x))
    );
  });

  ps.sort((a, b) => Math.abs(point.y - a.y) - Math.abs(point.y - b.y));
  const lowY = ps.filter(
    (p, i, arr) =>
      Math.min(Math.abs(point.y - p.y), Math.abs(p.y - point.y)) <=
      Math.min(Math.abs(point.y - arr[0].y), Math.abs(arr[0].y - point.y))
  );

  const possible = [...lowX, ...lowY];
  possible.sort((a, b) => {
    if (a.id < b.id) return -1;
    if (a.id > b.id) return 1;
    return 0;
  });
  return possible.filter((pt, i, arr) => i === 0 || pt.id !== arr[i - 1].id);
};

const ownership = (points) => {
  const pt = [...points];
  const bounds = searchSpace(points);
  const territory = [];

  for (let y = bounds.y; y < bounds.y + bounds.height; y++) {
    for (let x = bounds.x; x < bounds.x + bounds.width; x++) {
      const point = new Point(x, y);
      const closest = points.reduce((low, pos) => {
        if (pos.dist(point) === low.dist(point)) {
          return new Point(low.x, low.y, '.');
        }

        return pos.dist(point) < low.dist(point) ? pos : low;
      }, new Point(-999999, -999999, '_'));

      const index = pt.reduce(
        (index, el, i) => (el.id === closest.id ? i : index),
        -1
      );

      if (index !== -1) {
        pt[index].count++;

        if (x === bounds.x) pt[index].valid = false;
        if (y === bounds.y) pt[index].valid = false;
        if (x === bounds.width) pt[index].valid = false;
        if (y === bounds.height) pt[index].valid = false;
      }
    }
  }

  return pt;
};

const safeRegion = (points, threshold) => {
  const pt = [...points];
  const safe = [];
  const reject = [];
  const bounds = searchSpace(points);
  bounds.x -= 200;
  bounds.y -= 200;
  bounds.width += 200;
  bounds.height += 200;
  console.log(bounds);

  for (let y = bounds.y; y < bounds.height; y++) {
    for (let x = bounds.x; x < bounds.width; x++) {
      const point = new Point(x, y);
      let total = 0;

      for (let i = 0; i < points.length; i++) {
        total += point.dist(points[i]);
      }

      if (total < threshold) safe.push(point);
      else reject.push(total);
    }
  }

  reject.sort();
  console.log(reject);
  return safe;
};

export { Point, searchSpace, candidates, searchWhich, ownership, safeRegion };
