import { arePair, canReact, remove, react, findSmallest } from './';
import { polymer } from './day5.fixture.js';

describe('day 5', () => {
  it('determines whether two units are a reacting pair', () => {
    expect(arePair('a', 'A')).toBe(true);
    expect(arePair('b', 'B')).toBe(true);
    expect(arePair('a', 'a')).toBe(false);
    expect(arePair('a', 'b')).toBe(false);
    expect(arePair('a', 'B')).toBe(false);
  });

  it('determines whether a polymer has a reacting pair', () => {
    expect(canReact('aA')).toBe(true);
    expect(canReact('dabAcCaCBAcCcaDA')).toBe(true);
    expect(canReact('dabCBAcaDA')).toBe(false);
  });

  it('reacts polymer units', () => {
    expect(react('aA')).toBe('');
    expect(react('abBA')).toBe('');
    expect(react('abAB')).toBe('abAB');
    expect(react('aabAAB')).toBe('aabAAB');
    expect(react('dabAcCaCBAcCcaDA')).toBe('dabCBAcaDA');
  });

  it.skip('reacts the final polymer', () => {
    const reacted = react(polymer);
    console.log('length:', reacted.length);
  });

  it('removes a unit', () => {
    const test_polymer = 'dabAcCaCBAcCcaDA';

    expect(remove('A', test_polymer)).toBe('dbcCCBcCcD');
    expect(remove('B', test_polymer)).toBe('daAcCaCAcCcaDA');
  });

  it('finds the smallest test polymer', () => {
    const test_polymer = 'dabAcCaCBAcCcaDA';

    expect(findSmallest(test_polymer)).toBe(4);
  });

  it.skip('finds the final, fixed, polymer length', () => {
    const smallest = findSmallest(polymer);
    expect(smallest).toBe(5534);
  });
});
