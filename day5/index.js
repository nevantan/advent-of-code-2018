const canReact = (polymer) => {
  for (let i = 0; i < polymer.length - 1; i++) {
    if (arePair(polymer[i], polymer[i + 1])) return true;
  }
  return false;
};

const arePair = (a, b) => {
  return a !== b && a.toLowerCase() === b.toLowerCase();
};

const react = (polymer) => {
  while (canReact(polymer)) {
    for (let i = 0; i < polymer.length - 1; i++) {
      if (arePair(polymer[i], polymer[i + 1])) {
        polymer = polymer.slice(0, i) + polymer.slice(i + 2, polymer.length);
      }
    }
  }

  return polymer;
};

const remove = (unit, polymer) => {
  return polymer
    .split(unit)
    .join('')
    .split(unit.toLowerCase())
    .join('');
};

const findSmallest = (polymer) => {
  const units = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
  return units.reduce((low, unit) => {
    const length = react(remove(unit, polymer)).length;

    if (length < low || low === null) return length;

    return low;
  }, null);
};

export { arePair, canReact, remove, react, findSmallest };
