const parseClaim = (claim) => {
  const groups = claim
    .match(/^#(\d*)\s@\s(\d*),(\d*):\s(\d*)x(\d*)$/)
    .map((group) => parseInt(group));
  return {
    id: groups[1],
    x: groups[2],
    y: groups[3],
    w: groups[4],
    h: groups[5]
  };
};

const calcOverlap = (claim1, claim2) => {
  claim1 = parseClaim(claim1);
  claim2 = parseClaim(claim2);

  const overlapX = Math.max(
    0,
    Math.min(claim1.x + claim1.w, claim2.x + claim2.w) -
      Math.max(claim1.x, claim2.x)
  );
  const overlapY = Math.max(
    0,
    Math.min(claim1.y + claim1.h, claim2.y + claim2.h) -
      Math.max(claim1.y, claim2.y)
  );

  return overlapX * overlapY;
};

const calcAllOverlap = (claims, cb) => {
  claims = claims.map((claim) => parseClaim(claim));
  let count = 0;

  const top_sort = [...claims].sort((a, b) => {
    if (a.y < b.y) return -1;
    if (a.y > b.y) return 1;
    return 0;
  });

  const bot_sort = [...claims].sort((a, b) => {
    if (a.y + a.h < b.y + b.h) return -1;
    if (a.y + a.h > b.y + b.h) return 1;
    return 0;
  });

  const first = top_sort[0];
  const last = bot_sort[bot_sort.length - 1];

  for (let i = first.y; i < last.y + last.h; i++) {
    const scan_overlap = claims.filter(
      (claim) => i >= claim.y && i < claim.y + claim.h
    );

    // One or fewer claims on this scanline, skip
    if (scan_overlap.length <= 1) continue;

    const left_sort = [...scan_overlap].sort((a, b) => {
      if (a.x < b.x) return -1;
      if (a.x > b.x) return 1;
      return 0;
    });

    const right_sort = [...scan_overlap].sort((a, b) => {
      if (a.x + a.w < b.x + b.w) return -1;
      if (a.x + a.w > b.x + b.w) return 1;
      return 0;
    });

    const hfirst = left_sort[0];
    const hlast = right_sort[right_sort.length - 1];

    for (let x = hfirst.x; x < hlast.x + hlast.w; x++) {
      const cell_overlap = scan_overlap.filter(
        (claim) => x >= claim.x && x < claim.x + claim.w
      );

      if (cell_overlap.length > 1) {
        if (cb) cb(cell_overlap);
        count++;
      }
    }
  }

  return count;
};

// Work with process of elimination, remove claims as overlaps are found
const findOverlapFreeClaims = (claims) => {
  let claim_ids = claims.map((claim) => parseClaim(claim).id);

  calcAllOverlap(claims, (cell_overlap) => {
    claim_ids = claim_ids.filter(
      (id) => !cell_overlap.some((claim) => claim.id === id)
    );
  });

  return claim_ids[0];
};

export { calcOverlap, calcAllOverlap, findOverlapFreeClaims, parseClaim };
