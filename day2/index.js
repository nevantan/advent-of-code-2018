const checksum = (ids) => {
  const allCounts = ids.map((id) => countLetters(id));

  let twoLetter = 0;
  let threeLetter = 0;

  allCounts.forEach((counts) => {
    if (counts.some((count) => count === 2)) twoLetter++;
    if (counts.some((count) => count === 3)) threeLetter++;
  });

  return twoLetter * threeLetter;
};

const countLetters = (id) => {
  return Object.values(
    id.split('').reduce((acc, letter) => {
      if (acc[letter]) acc[letter]++;
      else acc[letter] = 1;

      return acc;
    }, {})
  ).sort();
};

const diff = (a, b) => {
  return a.length - common(a, b).length;
};

const common = (a, b) => {
  return a.split('').reduce((acc, letter, i) => {
    if (letter === b[i]) acc += letter;
    return acc;
  }, '');
};

const findAdjacent = (ids) => {
  ids.sort();

  for (let i = 0; i < ids.length; i++) {
    if (diff(ids[i], ids[i + 1]) === 1) {
      return common(ids[i], ids[i + 1]);
    }
  }
};

export { diff, common, findAdjacent, checksum, countLetters };
