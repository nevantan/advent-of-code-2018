const parseFrequency = (readings, cb) => {
  if (readings.length === 0) return 0;

  return readings.split(/\s+/).reduce((acc, mod) => {
    const next = acc + parseInt(mod);

    if (cb) cb(next);

    return next;
  }, 0);
};

const parseFrequencyDuplicate = (readings) => {
  if (readings.length === 0) return null;

  const changes = readings.split(/\s+/).map((change) => parseInt(change));
  let frequency = 0;
  let seen = { 0: true };

  while (true) {
    for (let i = 0; i < changes.length; i++) {
      frequency += changes[i];

      if (seen[frequency]) return frequency;

      seen[frequency] = true;
    }
  }

  return null;
};

const findDuplicate = (steps) => {
  let seen = {};
  for (let step of steps) {
    if (seen[step]) return step;

    seen[step] = true;
  }

  return null;
};

export { findDuplicate, parseFrequencyDuplicate };
export default parseFrequency;
